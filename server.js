const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const cors = require('cors');
const jwt = require('jsonwebtoken');

const Recipe = require('./models/Recipe');
const User = require('./models/User');

// Bring in GraphQL Express Middleware
const { graphiqlExpress, graphqlExpress } = require('apollo-server-express');
const { makeExecutableSchema } = require('graphql-tools');

const { typeDefs } = require('./schema');
const { resolvers } = require('./resolvers');

// Create a Schema
const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

// Connect to DB
mongoose.connect('mongodb://localhost:27017/react_recipes')
  .then(() => console.log('DB connected'))
  .catch(err => console.error(err));

// Init app
const app = express();

const corsOptions = {
  origin: 'http://localhost:3000/',
  credentials: true
};

app.use(cors(corsOptions));

// Setup JWT auth middleware
app.use(async (req, res, next) => {
  const token = req.headers['authorization'];
  if (token !== 'null') {
    try {
      const currentUser = await jwt.verify(token, 'aegdryqwgfdashfga78dsf');
      req.currentUser = currentUser;
      console.log(currentUser);
    } catch (err) {
      console.error(err);
    }
  }
  console.log(token);
  next();
});

// Create graphiql app
app.use('/graphiql', graphiqlExpress({endpointURL: '/graphql'}));

// Connect schemas to graphQl
app.use('/graphql', bodyParser.json(), graphqlExpress(({ currentUser }) => ({
  schema,
  context: {
    Recipe,
    User,
    currentUser
  }
})));

const PORT = process.env.PORT || 4444;

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`)
});