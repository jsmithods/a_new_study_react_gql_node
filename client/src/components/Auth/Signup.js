import React from 'react';

import Error from '../Error';

import { Mutation } from  'react-apollo';
import { SIGNUP_USER } from '../../queries';

const initialState = {
  username: "",
  email: "",
  password: "",
  passwordConfirmation: ""
};

class Signup extends React.Component {
  state = { ...initialState };

  clearState = () => {
    this.setState({ ...initialState });
  };

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  };

  validateForm = () => {
    const {username, email, password, passwordConfirmation} = this.state;
    const isInvalid = !username || !password || !email || password !== passwordConfirmation;

    return isInvalid;
  };

  handleSubmit = (event, signupUser) => {
    event.preventDefault();
    signupUser()
      .then(data => {
        let token = data.data.signupUser.token;
        localStorage.setItem('token', token);
        this.clearState();
      });
  };

  render() {
    const {username, email, password, passwordConfirmation} = this.state;

    return(
      <div className="App">
        <h2 className="App">Signup</h2>
        <Mutation mutation={SIGNUP_USER} variables={{ username, email, password }}>
          {( signupUser, { data, loading, error }) => {
            return (
              <form className="form" onSubmit={event => this.handleSubmit(event, signupUser)} >
                <input type="text" name="username" value={username} placeholder="Username" onChange={this.handleChange}/>
                <input type="email" name="email" value={email} placeholder="Email Address" onChange={this.handleChange}/>
                <input type="password" name="password" value={password} placeholder="Password" onChange={this.handleChange}/>
                <input type="password" name="passwordConfirmation" value={passwordConfirmation} placeholder="Confirm password" onChange={this.handleChange}/>
                <button
                  type="submit"
                  disabled={ loading || this.validateForm() }
                  className="button-primary"
                >Submit</button>
                {error && <Error error={error} />}
              </form>
            )
          }}
        </Mutation>
      </div>
    )
  }
}

export default Signup;